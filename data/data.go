package data

import "gitee.com/wang1974/go-grpcservice/pb"

var Employees = []pb.Employee{
	{
		Id:        1,
		No:        1994,
		FirstName: "chn",
		LastName:  "bin",
	},
	{
		Id:        2,
		No:        1996,
		FirstName: "wyh",
		LastName:  "zxf",
	},
}
