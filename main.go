package main

import (
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitee.com/wang1974/go-grpcservice/pb"
	s "gitee.com/wang1974/go-grpcservice/server"
	"gitee.com/wang1974/go-grpcservice/service"
)

const (
	APPNAME = "grpc-service"
	port    = "8081"
)

func main() {
	// 证书
	creds, err := credentials.NewServerTLSFromFile("cert.pem", "key.pem")
	if err != nil {
		log.Fatalf("err: %s", err.Error())
	}
	// 传递进grpc
	options := []grpc.ServerOption{grpc.Creds(creds), grpc.MaxRecvMsgSize(1024 * 1024), grpc.UnaryInterceptor(service.AuthGrpcInterceptor)}
	// grpc服务  ...展开符
	server := grpc.NewServer(options...)
	//服务注册
	pb.RegisterEmployeeServiceServer(server, &s.EmployeeService{})

	log.Println("grpc server started..." + port)
	tcp, err := net.ResolveTCPAddr("tcp", "0.0.0.0:"+port)
	if err != nil {
		log.Fatal(err.Error())
	}
	tcpListener, err := net.ListenTCP("tcp", tcp)
	if err != nil {
		log.Fatal(err.Error())
	}
	server.Serve(tcpListener)
}
