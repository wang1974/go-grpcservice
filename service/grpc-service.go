package service

import (
	"context"

	"google.golang.org/grpc"
)

func AuthGrpcInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
	return nil, nil
}
