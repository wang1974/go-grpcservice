### grpc-server

- 获取组件
- go get -u google.golang.org/grpc

- 生成 pb 文件
- protoc --proto_path=./protos ./protos/\*.proto --go_out=plugins=grpc:./pb

- 生成 ssl 证书
- cmd: openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes -subj /CN=localhost

- go1.15 之后需使用临时配置 添加扩展 SAN
- 临时配置 openssl.cnf 并使用
- -config 指定配置文件
- -extensions v3_req 扩展
- cmd: openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes -subj /CN=localhost -config ./openssl.cnf -extensions v3_req
